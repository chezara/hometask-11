'use strict';
/*
    В этом задании нельзя менять разметку, только писать код в этом файле.
 */

/**
*   1. Удали со страницы элемент с id "deleteMe"
**/

function removeBlock() {
  const blockDel = document.getElementById('deleteMe');
  blockDel.remove();
}

/**
 *  2. Сделай так, чтобы во всех элементах с классом wrapper остался только один параграф,
 *  в котором будет сумма чисел из всех параграфов.
 *
 *  Например, такой элемент:
 *
 *  <div class="wrapper"><p>5</p><p>15</p><p>25</p><p>35</p></div>
 *
 *  должен стать таким
 *
 *  <div class="wrapper"><p>80</p></div>
 */

function calcParagraphs() {
  const wraps = document.querySelectorAll('.wrapper');

  for (let i = 0; i < wraps.length; i++) {

    const pElements = wraps[i].getElementsByTagName('p');
    let sum = 0;

    for (let j = 0; j < pElements.length; j++) {
        const num = Number(pElements[j].innerHTML);
        sum = sum + num;
    }

    wraps[i].innerHTML = '';
    wraps[i].innerHTML = '<p>' + sum + '</p>';
  }
}


/**
 *  3. Измени value и type у input c id "changeMe"
 *
 */

function changeInput() {
  const input = document.getElementById('changeMe');
  input.setAttribute('value', 'здесь был вася');
  input.setAttribute('type', 'text');
}

/**
 *  4. Используя функции appendChild и insertBefore дополни список с id "changeChild"
 *  чтобы получилась последовательность <li>0</li><li>1</li><li>2</li><li>3</li>
 *
 */

function appendList() {
  const parentEl = document.getElementById('changeChild');
  const firstChild = parentEl.children[1];
  const li1 = document.createElement("li");
  const li3 = document.createElement("li");
  li1.innerHTML = '1';
  li3.innerHTML = '3';
  
  parentEl.appendChild(li3);
  parentEl.insertBefore(li1, firstChild);
}

/**
 *  5. Поменяй цвет всем div с class "item".
 *  Если у блока class "red", то замени его на "blue" и наоборот для блоков с class "blue"
 */

function changeColors() {
  const block = document.getElementById('blocks');
  const nodes = block.children;
  const blockChildren = block.getElementsByTagName('div');
  for (let i = 0; i < nodes.length; i++) { 
    if (blockChildren[i].className == 'item red') {
      blockChildren[i].className = 'item blue';
    } else {
      blockChildren[i].className = 'item red';
    }
  }
}

removeBlock();
calcParagraphs();
changeInput();
appendList();
changeColors();